#ifndef SHARED_QUEUE_H
#define SHARED_QUEUE_H

#include <queue>
#include <boost/thread.hpp>

template<typename T>
class SharedQueue
{

private:
  std::queue<T> queue_;
  mutable boost::mutex mutex_;
  boost::condition_variable cond_;

public:
  void push(const T& msg)
  {
    boost::mutex::scoped_lock lock(mutex_);
    queue_.push(msg);
    cond_.notify_one();
    lock.unlock();
  }

  bool pop_nowait(T& msg)
  {
    boost::mutex::scoped_lock lock(mutex_);
    if (!queue_.empty()){
      msg = queue_.front();
      queue_.pop();
      return true;
    }
    return false;
  }

};

#endif

