#include <iostream>

#include <netdb.h> // socket
#include <errno.h>

#include <rdma/rsocket.h>

#include <boost/thread.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

size_t const buffer_size = 10240;
size_t const repetitions = 1024;

int connect_to_server(char* address, char* port)
{
  addrinfo* res;
  if (getaddrinfo(address, port, NULL, &res)) {
    std::cout << "getaddrinfo\t[ERROR] >> errno " << errno << std::endl;
    return -1;
  } else {
    std::cout << "getaddrinfo\t[OK]" << std::endl;
  }

  int const socket_fd = rsocket(res->ai_family, res->ai_socktype,
      res->ai_protocol);
  if (socket_fd == -1) {
    std::cout << "rsocket\t\t[ERROR] >> errno " << errno << std::endl;
    return -1;
  } else {
    std::cout << "rsocket\t\t[OK]" << std::endl;
  }

  if (rconnect(socket_fd, res->ai_addr, res->ai_addrlen)) {
    std::cout << "rconnect\t[ERROR] >> errno " << errno << std::endl;
    return -1;
  } else {
    std::cout << "rconnect\t[OK]" << std::endl;
  }

  return socket_fd;
}

int main()
{
  int const socket_fd = connect_to_server("192.168.1.13", "7427");
  if (socket_fd < 0) {
    return socket_fd;
  }

  char msg[buffer_size];
  for (size_t t = 0; t < buffer_size; ++t) {
    msg[t] = 3;
  }

  // ONE SEND AND THEN ONE RECEIVE
  {
    boost::posix_time::ptime const t1 =
        boost::posix_time::microsec_clock::local_time();

    for (size_t i = 0; i < repetitions; ++i) {
      ssize_t const bytes_sent = rwrite(socket_fd, msg, buffer_size);
      if (bytes_sent != buffer_size) {
        std::cout << "bytes_sent: " << bytes_sent << " errno: " << errno
            << std::endl;
      }
      if (bytes_sent == -1) {
        break;
      }
      ssize_t const bytes_received = rread(socket_fd, msg, buffer_size);
      if (bytes_received != buffer_size) {
        std::cout << "bytes_received: " << bytes_received << " errno: " << errno
            << std::endl;
      }
      if (bytes_received <= 0) {
        break;
      }
    }

    boost::posix_time::ptime const t2 =
        boost::posix_time::microsec_clock::local_time();

    boost::posix_time::time_duration const diff = t2 - t1;

    std::cout << "One send and one receive " << repetitions << " times of a "
        << buffer_size << " bytes message\n";

    std::cout << "Latency of "
        << (static_cast<double>(diff.total_microseconds())
            / static_cast<double>(repetitions)) * 2.0 << " us/msg\n";
  }

  // ALL SENDS AND THEN ALL RECEIVES
  {
    boost::posix_time::ptime const t1 =
        boost::posix_time::microsec_clock::local_time();

    for (size_t i = 0; i < repetitions; ++i) {
      ssize_t const bytes_sent = rwrite(socket_fd, msg, buffer_size);
      if (bytes_sent != buffer_size) {
        std::cout << "bytes_sent: " << bytes_sent << " errno: " << errno
            << std::endl;
      }
      if (bytes_sent == -1) {
        break;
      }
    }
    for (size_t i = 0; i < repetitions; ++i) {
      ssize_t const bytes_received = rread(socket_fd, msg, buffer_size);
      if (bytes_received != buffer_size) {
        std::cout << "bytes_received: " << bytes_received << " errno: " << errno
            << std::endl;
      }
      if (bytes_received <= 0) {
        break;
      }
    }

    boost::posix_time::ptime const t2 =
        boost::posix_time::microsec_clock::local_time();

    boost::posix_time::time_duration const diff = t2 - t1;

    std::cout << "First " << repetitions << " sends and then " << repetitions
        << " receives of a " << buffer_size << " bytes message\n";

    std::cout << "Latency of "
        << (static_cast<double>(diff.total_microseconds())
            / static_cast<double>(repetitions)) * 2.0 << " us/msg\n";
  }

  rshutdown(socket_fd, SHUT_RDWR);
  rclose(socket_fd);
  return 0;
}
