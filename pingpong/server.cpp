#include <iostream>
#include <climits>

#include <netdb.h> // socket
#include <errno.h>

#include <rdma/rsocket.h>

#include <boost/thread.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

size_t const buffer_size = 10240;
size_t const repetitions = 1024;

int listen(char* port)
{
  addrinfo hints;
  memset(&hints, 0, sizeof(hints));
  hints.ai_flags = RAI_PASSIVE;
  addrinfo* res;
  if (getaddrinfo(NULL, port, &hints, &res)) {
    std::cout << "getaddrinfo\t[ERROR] >> errno " << errno << std::endl;
    return -1;
  } else {
    std::cout << "getaddrinfo\t[OK]" << std::endl;
  }

  int const socket_fd = rsocket(res->ai_family, res->ai_socktype,
      res->ai_protocol);
  if (socket_fd == -1) {
    std::cout << "rsocket\t\t[ERROR] >> errno " << errno << std::endl;
    return -1;
  } else {
    std::cout << "rsocket\t\t[OK]" << std::endl;
  }

  int const opt_val = 1;
  rsetsockopt(socket_fd, SOL_SOCKET, SO_REUSEADDR, &opt_val, sizeof(opt_val));

  if (rbind(socket_fd, res->ai_addr, res->ai_addrlen)) {
    std::cout << "rbind\t\t[ERROR] >> errno " << errno << std::endl;
    rclose(socket_fd);
    freeaddrinfo(res);
    return -1;
  } else {
    std::cout << "rbind\t\t[OK]" << std::endl;
  }

  if (rlisten(socket_fd, INT_MAX)) {
    std::cout << "rlisten\t\t[ERROR] >> errno " << errno << std::endl;
    rclose(socket_fd);
    freeaddrinfo(res);
    return -1;
  } else {
    std::cout << "rlisten\t\t[OK]" << std::endl;
  }

  freeaddrinfo(res);
  return socket_fd;

}

int accept(int socket_fd)
{
  union rsocket_address
  {
    sockaddr sa;
    sockaddr_in sin;
    sockaddr_in6 sin6;
    sockaddr_storage storage;
  } rsa;
  socklen_t len = sizeof(rsocket_address);

  int new_socket_fd = raccept(socket_fd, &rsa.sa, &len);

  if (new_socket_fd == -1) {
    std::cout << "raccept\t\t[ERROR] >> errno " << errno << std::endl;
  } else {
    std::cout << "raccept\t\t[OK]" << std::endl;
  }
  return new_socket_fd;
}

int main()
{
  int listening_socket = listen("7427");
  if (listening_socket < 0) {
    return -1;
  }

  int new_socket = accept(listening_socket);

  char msg[buffer_size];

  for (size_t i = 0; i < repetitions; ++i) {
    ssize_t const bytes_received = rread(new_socket, msg, buffer_size);
    if(bytes_received <= 0) {
      break;
    }
    ssize_t const bytes_sent = rwrite(new_socket, msg, buffer_size);
    if(bytes_sent == -1) {
      break;
    }
  }

  for (size_t i = 0; i < repetitions; ++i) {
    ssize_t const bytes_received = rread(new_socket, msg, buffer_size);
    if(bytes_received <= 0) {
      break;
    }
  }
  for (size_t i = 0; i < repetitions; ++i) {
    ssize_t const bytes_sent = rwrite(new_socket, msg, buffer_size);
    if(bytes_sent == -1) {
      break;
    }
  }

  rclose(new_socket);
  rclose(listening_socket);
  return 0;
}
