#ifndef UTILITIES_COMMONS_H
#define UTILITIES_COMMONS_H

#include <rdma/rdma_cma.h>
#include <rdma/rdma_verbs.h>

#define TEST_NZ(x) do { if ( (x)) die("error: " #x " failed (returned non-zero)." ); } while (0)
#define TEST_Z(x)  do { if (!(x)) die("error: " #x " failed (returned zero/null)."); } while (0)

const size_t BUFFER_SIZE = 1024;
const int TIMEOUT_IN_MS = 500; /* ms */

struct context
{
  ibv_context* ctx;
  ibv_pd* pd;
  ibv_cq* cq;
  ibv_comp_channel* comp_channel;

  pthread_t cq_poller_thread;
};

struct connection
{
  ibv_qp* qp;

  ibv_mr* recv_mr;
  ibv_mr* send_mr;

  char* recv_region;
  char* send_region;
};

void die(const std::string&);

void build_connection(rdma_cm_id*); // both client and server
void accept_connection(rdma_cm_id*); // only server
void send_message(rdma_cm_id*, const char*, size_t);
void free_resources(rdma_cm_id*); // both client and server

rdma_event_channel* create_event_channel();
rdma_cm_id* create_cm_id(rdma_event_channel*);
void bind_address(rdma_cm_id*, char*);
void listen_connections(rdma_cm_id*);
void resolve_address(rdma_cm_id*, char*, char*);
void resolve_route(rdma_cm_id*);
void request_connection(rdma_cm_id*);
#endif
