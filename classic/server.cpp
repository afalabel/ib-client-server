#include <iostream>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <rdma/rdma_cma.h>

#include <boost/thread.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

#include "commons.h"

int on_event(rdma_cm_event& event)
{
  switch (event.event) {
  case RDMA_CM_EVENT_CONNECT_REQUEST:
    build_connection(event.id);
    accept_connection(event.id);
    break;
  case RDMA_CM_EVENT_ESTABLISHED:
    // Here I can... do what?? I don't know...
    break;
  case RDMA_CM_EVENT_DISCONNECTED:
    free_resources(event.id);
    break;
  default:
    std::cout << rdma_event_str(event.event) << " not handled." << std::endl;
    return 1;
  }

  return 0;
}

void cm_event_handler(rdma_event_channel* ec)
{
  rdma_cm_event* event = NULL;
  while (rdma_get_cm_event(ec, &event) == 0) {
    rdma_cm_event event_copy = *event;
    rdma_ack_cm_event(event);
    if (on_event(event_copy)) {
      break;
    }
  }
}

int main()
{
  rdma_event_channel* ec = create_event_channel();
  rdma_cm_id* id = create_cm_id(ec);

  bind_address(id, "20079");
  listen_connections(id);

  boost::thread cm_event_handler_thread(cm_event_handler, ec);
  cm_event_handler_thread.join();


  rdma_destroy_id(id);
  rdma_destroy_event_channel(ec);

  return 0;
}
