#include <iostream>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netdb.h>

#include <climits>

#include <errno.h>

#include "commons.h"

void die(const std::string& reason)
{
  std::cerr << reason << std::endl;
  exit (EXIT_FAILURE);
}

static void on_completion(ibv_wc* wc)
{
  if (wc->status != IBV_WC_SUCCESS) {
    std::cout << "wr_id: " << wc->wr_id << std::endl;
    std::cout << "status: " << ibv_wc_status_str(wc->status) << std::endl;
    std::cout << "qp_num: " << wc->qp_num << std::endl;
    std::cout << "vendor_err: " << wc->vendor_err << std::endl;
    die("on_completion: status is not IBV_WC_SUCCESS.");
  }
  connection* conn = (connection*) (uintptr_t) wc->wr_id;
  switch (wc->opcode) {
  case IBV_WC_RECV:
    /* Handler of RECV_DONE*/
    std::cout << "--> IBV_WC_RECV" << std::endl;
    //std::cout << "received " << wc->byte_len << " bytes" << std::endl;
    std::cout << "received message: " << conn->recv_region << std::endl;
    break;
  case IBV_WC_SEND:
    /* Handler of IBV_WC_SEND*/
    std::cout << "--> IBV_WC_SEND" << std::endl;
    // In IBV_WC_SEND wc->byte_len is not guaranteed to be correct
    //std::cout << "sent " << wc->byte_len << " bytes" << std::endl;
    break;
  default:
    die("on_completion: completion isn't a send or a receive.");
  }
}

static void* poll_cq(void* s_ctx)
{
  while (1) {
    ibv_cq* cq;
    void* ev_ctx;
    TEST_NZ(ibv_get_cq_event(((context* ) s_ctx)->comp_channel, &cq, &ev_ctx));
    // All completion events that ibv_get_cq_event() returns must be
    // acknowledged using ibv_ack_cq_events().
    ibv_ack_cq_events(cq, 1);

    TEST_NZ(ibv_req_notify_cq(cq, 0));

    ibv_wc wc;
    while (ibv_poll_cq(cq, 1, &wc)) {
      on_completion(&wc);
    }
  }
  return NULL;
}

static context* build_context(ibv_context* verbs)
{
  context* s_ctx = (context*) malloc(sizeof(context));
  s_ctx->ctx = verbs;
  TEST_Z(s_ctx->pd = ibv_alloc_pd(s_ctx->ctx));
  TEST_Z(s_ctx->comp_channel = ibv_create_comp_channel(s_ctx->ctx));
  TEST_Z(
      s_ctx->cq = ibv_create_cq(s_ctx->ctx, 10, NULL, s_ctx->comp_channel, 0)); /* cqe=10 is arbitrary */
  TEST_NZ(ibv_req_notify_cq(s_ctx->cq, 0));
  TEST_NZ(
      pthread_create(&s_ctx->cq_poller_thread, NULL, poll_cq, (void* ) s_ctx));
  return s_ctx;
}

static void build_qp_attr(ibv_qp_init_attr* qp_attr, ibv_cq* cq)
{
  memset(qp_attr, 0, sizeof(*qp_attr));

  qp_attr->send_cq = cq; /* CQ to be associated with the Send Queue (SQ) */
  qp_attr->recv_cq = cq; /* CQ to be associated with the Receive Queue (RQ) */
  qp_attr->qp_type = IBV_QPT_RC; /* QP Transport Service Type: IBV_QPT_RC, IBV_QPT_UC, or IBV_QPT_UD */

  qp_attr->cap.max_send_wr = 10; /* Requested max number of outstanding WRs in the SQ */
  qp_attr->cap.max_recv_wr = 10; /* Requested max number of outstanding WRs in the RQ */
  qp_attr->cap.max_send_sge = 1; /* Requested max number of scatter/gather (s/g) elements in a WR in the SQ */
  qp_attr->cap.max_recv_sge = 1; /* Requested max number of s/g elements in a WR in the RQ */

}

static void alloc_resources(connection* conn, ibv_pd* pd)
{
  conn->send_region = (char*) malloc(BUFFER_SIZE);
  conn->recv_region = (char*) malloc(BUFFER_SIZE);

  TEST_Z(
      conn->send_mr = ibv_reg_mr(pd, conn->send_region, BUFFER_SIZE,
          IBV_ACCESS_LOCAL_WRITE | IBV_ACCESS_REMOTE_WRITE));

  TEST_Z(
      conn->recv_mr = ibv_reg_mr(pd, conn->recv_region, BUFFER_SIZE,
          IBV_ACCESS_LOCAL_WRITE | IBV_ACCESS_REMOTE_WRITE));
}

static void post_receive(connection* conn)
{
  ibv_sge sge;
  sge.addr = (uintptr_t) conn->recv_region; /* Start address of the local memory buffer */
  sge.length = BUFFER_SIZE; /* Length of the buffer */
  sge.lkey = conn->recv_mr->lkey; /* Key of the local Memory Region */

  ibv_recv_wr wr2;
  wr2.wr_id = (uintptr_t) conn; /* User defined WR ID */
  wr2.next = NULL; /* Pointer to next WR in list, NULL if last WR */
  wr2.sg_list = &sge; /* Pointer to the s/g array */
  wr2.num_sge = 1; /* Size of the s/g array */

  ibv_recv_wr wr1;
  wr1.wr_id = (uintptr_t) conn; /* User defined WR ID */
  wr1.next = &wr2; /* Pointer to next WR in list, NULL if last WR */
  wr1.sg_list = &sge; /* Pointer to the s/g array */
  wr1.num_sge = 1; /* Size of the s/g array */

  ibv_recv_wr* bad_wr = NULL;
  /*
   ibv_post_recv() posts the linked list of work requests (WRs) starting with
   wr to the receive queue of the queue pair qp. It stops processing WRs from
   this list at the first failure (that can be detected immediately while
   requests are being posted), and returns this failing WR through bad_wr.
   */
  TEST_NZ(ibv_post_recv(conn->qp, &wr1, &bad_wr));
}

void build_connection(rdma_cm_id* id)
{
  context* s_ctx = build_context(id->verbs);
  ibv_qp_init_attr qp_attr;
  build_qp_attr(&qp_attr, s_ctx->cq);

  TEST_NZ(rdma_create_qp(id, s_ctx->pd, &qp_attr));

  // setting up connection object
  connection* conn = (connection*) malloc(sizeof(connection));
  conn->qp = id->qp;
  alloc_resources(conn, s_ctx->pd);
  post_receive(conn);

  id->context = conn;
}

void accept_connection(rdma_cm_id* id)
{
  rdma_conn_param cm_params;
  memset(&cm_params, 0, sizeof(cm_params));
  TEST_NZ(rdma_accept(id, &cm_params));
}

void send_message(rdma_cm_id* id, const char* msg, size_t size)
{
  if (size > BUFFER_SIZE) {
    die("send_message: size of message greater than BUFFER_SIZE");
  }

  std::cout << "sending message: " << msg << std::endl;

  connection* conn = (connection*) id->context;
  memcpy(conn->send_region, msg, size);

  ibv_sge sge;
  sge.addr = (uintptr_t) conn->send_region; /* Start address of the local memory buffer */
  sge.length = size; /* Length of the buffer */
  sge.lkey = conn->send_mr->lkey; /* Key of the local Memory Region */

  ibv_send_wr wr;
  memset(&wr, 0, sizeof(wr)); /* set bits of all fields to 0 */
  wr.opcode = IBV_WR_SEND; /* Operation type */
  wr.sg_list = &sge; /* Pointer to the s/g array */
  wr.num_sge = 1; /* Size of the s/g array */
  wr.send_flags = IBV_SEND_SIGNALED; /* Flags of the WR properties */
  // there are other fields in ibv_send_wr structure...

  ibv_send_wr* bad_wr = NULL;
  TEST_NZ(ibv_post_send(conn->qp, &wr, &bad_wr));
}

void free_resources(rdma_cm_id* id)
{
  connection* conn = (connection*) id->context;
  rdma_destroy_qp(id);

  ibv_dereg_mr(conn->send_mr);
  ibv_dereg_mr(conn->recv_mr);

  free(conn->send_region);
  free(conn->recv_region);

  free(conn);

  rdma_destroy_id(id);
}

// New functions

rdma_event_channel* create_event_channel()
{
  rdma_event_channel* ec = rdma_create_event_channel();
  if (!ec) {
    std::cout << "create_event_channel\t[ERROR] >> errno " << errno
        << std::endl;
  } else {
    std::cout << "create_event_channel\t[OK]" << std::endl;
  }
  return ec;
}

rdma_cm_id* create_cm_id(rdma_event_channel* ec)
{
  rdma_cm_id* id = NULL;
  if (rdma_create_id(ec, &id, NULL, RDMA_PS_TCP)) {
    std::cout << "rdma_create_id\t\t[ERROR] >> errno " << errno << std::endl;
  } else {
    std::cout << "rdma_create_id\t\t[OK]" << std::endl;
  }
  return id;
}

rdma_addrinfo* get_address_info(char* host, char* port)
{
  rdma_addrinfo* address;
  rdma_addrinfo hints;
  memset(&hints, 0, sizeof(hints));
  hints.ai_port_space = RDMA_PS_TCP;
  // if host is null it should be the server
  if (!host) {
    hints.ai_flags = RAI_PASSIVE;
  }
  if (rdma_getaddrinfo(host, port, &hints, &address)) {
    std::cout << "rdma_getaddrinfo\t[ERROR] >> errno " << errno << std::endl;
  } else {
    std::cout << "rdma_getaddrinfo\t[OK]" << std::endl;
  }
  return address;
}

void bind_address(rdma_cm_id* id, char* port)
{
  rdma_addrinfo* address = get_address_info(NULL, port);
  if (rdma_bind_addr(id, address->ai_src_addr)) {
    std::cout << "rdma_bind_addr\t\t[ERROR] >> errno " << errno << std::endl;
  } else {
    std::cout << "rdma_bind_addr\t\t[OK]" << std::endl;
  }
}

void listen_connections(rdma_cm_id* id)
{
  /* INT_MAX maximum number of connections queued */
  if (rdma_listen(id, INT_MAX)) {
    std::cout << "rdma_listen\t\t[ERROR] >> errno " << errno << std::endl;
  } else {
    std::cout << "rdma_listen\t\t[OK]" << std::endl;
  }
}

void resolve_address(rdma_cm_id* id, char* host, char* port)
{
  addrinfo* addr;
  getaddrinfo(host, port, NULL, &addr);
  /* INT_MAX maximum number of ms before timeout*/
  if (rdma_resolve_addr(id, NULL, addr->ai_addr, INT_MAX)) {
    std::cout << "rdma_resolve_addr\t[ERROR] >> errno " << errno << std::endl;
  } else {
    std::cout << "rdma_resolve_addr\t[OK]" << std::endl;
  }
  freeaddrinfo(addr);
}

void resolve_route(rdma_cm_id* id)
{
  /* INT_MAX maximum number of connections queued */
  if (rdma_resolve_route(id, INT_MAX)) {
    std::cout << "rdma_resolve_route\t[ERROR] >> errno " << errno << std::endl;
  } else {
    std::cout << "rdma_resolve_route\t[OK]" << std::endl;
  }
}

void request_connection(rdma_cm_id* id)
{
  rdma_conn_param cm_params;
  memset(&cm_params, 0, sizeof(cm_params));
  if (rdma_connect(id, &cm_params)) {
    std::cout << "rdma_connect\t\t[ERROR] >> errno " << errno << std::endl;
  } else {
    std::cout << "rdma_connect\t\t[OK]" << std::endl;
  }
}

