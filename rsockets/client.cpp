#include <iostream>

#include <netdb.h> // socket
#include <errno.h>

#include "rsocketmm.h"

#include <boost/thread.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/program_options.hpp>

#include "logging.hpp"

namespace po = boost::program_options;

int connect_to_server(std::string address, std::string port)
{
  addrinfo* res;
  if (getaddrinfo(address.c_str(), port.c_str(), NULL, &res)) {
    std::cout << "getaddrinfo\t[ERROR] >> errno " << errno << std::endl;
    return -1;
  }

  int const socket_fd = rsocket(res->ai_family, res->ai_socktype,
      res->ai_protocol);
  if (socket_fd == -1) {
    std::cout << "rsocket\t\t[ERROR] >> errno " << errno << std::endl;
    return -1;
  }

  if (rconnect(socket_fd, res->ai_addr, res->ai_addrlen)) {
    std::cout << "rconnect\t[ERROR] >> errno " << errno << std::endl;
    return -1;
  }

  return socket_fd;
}

void function_thread(std::string address, std::string port, size_t buffer_size,
    size_t repetitions)
{
  int const socket_fd = connect_to_server(address, port);
  if (socket_fd < 0) {
    return;
  }

  char msg[buffer_size];
  msg[0] = '6';
  msg[1] = '\0';

  for (size_t i = 0; i < repetitions; ++i) {
    ssize_t const res = rsend(socket_fd, msg, buffer_size, 0);
    if (res != buffer_size) {
      std::cout << "res: " << res << " errno: " << errno << std::endl;
      return;
    }
  }

  rshutdown(socket_fd, SHUT_RDWR);
  rclose(socket_fd);
}

int main(int argc, char* argv[])
{
  std::string address;
  std::string port;
  size_t threads_number;
  size_t buffer_size;
  size_t repetitions;

  po::options_description desc("Options");
  desc.add_options()("help,h", "Print help messages.")
    	    		("debug,d", "Enable debug level.")
				    ("address,a",
				    		po::value < std::string > (&address)->default_value("192.168.1.13"),"Address.")
				    ("port,p",
				    		po::value < std::string >(&port)->default_value("7427"), "Port.")
  	  	  	  	  	("threads,t",
  	  	  	  	  			po::value < size_t > (&threads_number)->default_value(20),"Number of threads.")
  	  	  	  	  	("size,s",
  	  	  	  	  			po::value < size_t > (&buffer_size)->default_value(2 << 12),"Size of message in bytes.")
  	  	  	  	  	("repetitions,r",
  	  	  	  	  			po::value < size_t > (&repetitions)->default_value(100000),"Number of send repetitions.");

  try {
    po::variables_map vm;
    po::store(po::command_line_parser(argc, argv).options(desc).run(), vm);
    po::notify(vm);

    if (vm.count("help")) {
      std::cout << desc << std::endl;
      return 0;
    }
    if (vm.count("debug")) {
    	PrintLog::ReportingLevel() = PrintLog::FromString("DEBUG");
    }

  } catch (const po::error& e) {
    std::cerr << "Error: " << e.what() << std::endl;
    std::cerr << desc << std::endl;
    return -1;
  }

  boost::thread_group threads;

  boost::posix_time::ptime const t1 =
      boost::posix_time::microsec_clock::local_time();

  for (size_t i = 0; i < threads_number; ++i) {
    threads.add_thread(
        new boost::thread(function_thread, address, port, buffer_size,
            repetitions));
  }

  threads.join_all();

  boost::posix_time::ptime const t2 =
      boost::posix_time::microsec_clock::local_time();

  boost::posix_time::time_duration const diff = t2 - t1;

  size_t const megabytes = buffer_size * repetitions * threads_number / 1000000;
  size_t const milliseconds = diff.total_milliseconds();

  double const rate = 8 * ((double) megabytes / 1000.)
      / ((double) milliseconds / 1000.);

  std::cout << "Bandwidth: " << rate << " Gb/sec\n";

  return 0;
}
