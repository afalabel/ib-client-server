#include <iostream>
#include <algorithm>
#include <climits>

#include <netdb.h> // socket
#include <errno.h>
#include <sys/fcntl.h>

#include "rsocketmm.h"

#include <boost/thread.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/program_options.hpp>

#include "shared_queue.hpp"
#include "logging.hpp"

namespace po = boost::program_options;

int listen(std::string port)
{
  addrinfo hints;
  memset(&hints, 0, sizeof(hints));
  hints.ai_flags = RAI_PASSIVE;
  addrinfo* res;
  if (getaddrinfo(NULL, port.c_str(), &hints, &res)) {
    std::cout << "getaddrinfo\t[ERROR] >> errno " << errno << std::endl;
    return -1;
  }

  int const socket_fd = rsocket(res->ai_family, res->ai_socktype,
      res->ai_protocol);
  if (socket_fd == -1) {
    std::cout << "rsocket\t\t[ERROR] >> errno " << errno << std::endl;
    return -1;
  }

  int const opt_val = 1;
  if (rsetsockopt(socket_fd, SOL_SOCKET, SO_REUSEADDR, &opt_val,
      sizeof(opt_val))) {
    std::cout << "rsetsockopt\t[ERROR] >> errno " << errno << std::endl;
    rclose(socket_fd);
    freeaddrinfo(res);
    return -1;
  }

  if (rbind(socket_fd, res->ai_addr, res->ai_addrlen)) {
    std::cout << "rbind\t\t[ERROR] >> errno " << errno << std::endl;
    rclose(socket_fd);
    freeaddrinfo(res);
    return -1;
  }

  if (rlisten(socket_fd, INT_MAX)) {
    std::cout << "rlisten\t\t[ERROR] >> errno " << errno << std::endl;
    rclose(socket_fd);
    freeaddrinfo(res);
    return -1;
  }

  freeaddrinfo(res);
  return socket_fd;

}

int accept(int socket_fd)
{
  int new_socket_fd = -1;
  do {
    new_socket_fd = raccept(socket_fd, NULL, NULL);
  } while (new_socket_fd == -1 && errno == EWOULDBLOCK);

  if (new_socket_fd == -1) {
    std::cout << "raccept\t\t[ERROR] >> errno " << errno << std::endl;
  }

  return new_socket_fd;
}

bool is_dec(size_t i, size_t j)
{
  return (i > j);
}

void requests_handler(SharedQueue<pollfd>* queue, size_t const buffer_size)
{

  std::vector < pollfd > polling_fds;

  char msg[buffer_size];

  while (1) {

    std::vector < size_t > fds_to_remove;

    // rpoll
    int const events_num = rpoll(&polling_fds.front(), polling_fds.size(), 0);

    if (events_num) {
      // iteration over fd
      size_t const current_fds_size = polling_fds.size();
      //Print_LOG(logDEBUG) <<"Current file descriptors vector size  "<<current_fds_size;
      for (size_t i = 0; i < current_fds_size; ++i) {
    	//Print_LOG(logDEBUG) <<"Polling file descriptor  "<<polling_fds[i].fd;
        if (polling_fds[i].revents) {
          // if for this fd there is an event
          if (polling_fds[i].revents & POLLIN) {
            // new data to read
            int const bytes_received = rrecv(polling_fds[i].fd, msg,
                buffer_size, 0);
            if (bytes_received > 0) {
               //Print_LOG(logDEBUG) <<"Received  "<<bytes_received<<" bytes";
               //Print_LOG(logDEBUG) <<"Message  "<<msg;
            } else if (bytes_received == 0) {
              //Print_LOG(logDEBUG) <<"Client disconnetted gracefully";
              fds_to_remove.push_back(i);
            } else if (!(errno == EAGAIN || errno == EWOULDBLOCK)) {
              // here you can check errno if you want
              fds_to_remove.push_back(i);
              std::cout << "rrecv\t" << bytes_received << "\t[ERROR] >> errno "
                  << errno << std::endl;
            }
          } else {
            std::cerr << "Unhandled event: size=" << polling_fds.size() << " i="
                << i << " revents=" << polling_fds[i].revents << std::endl;
          }
        }
      }
    } else if (events_num == -1) {
      std::cerr << "rpoll\t\t[ERROR] >> errno " << errno << std::endl;
    }

    // Reorder fds_to_remove (DEC)
    std::sort (fds_to_remove.begin(), fds_to_remove.end(), is_dec);

    // remove closed fd
    for (size_t i = 0; i < fds_to_remove.size(); ++i) {
      rshutdown(polling_fds[fds_to_remove[i]].fd, SHUT_RDWR);
      rclose(polling_fds[fds_to_remove[i]].fd);
      polling_fds.erase(polling_fds.begin() + fds_to_remove[i]);
    }

    // Check for new sockets to add (nonblocking)
    pollfd p;
    if (queue->pop_nowait(p)) {
      polling_fds.push_back(p);
    }

  }
}

int main(int argc, char* argv[])
{

  std::string port;
  size_t buffer_size;
  size_t threads_number;

  po::options_description desc("Options");
  desc.add_options()
      ("help,h", "Print help messages.")
      ("debug,d", "Enable debug level.")
      ("port,p",po::value < std::string > (&port)->default_value("7427"), "Port.")
      ("size,s", po::value < size_t > (&buffer_size)->default_value(2 << 12),"Size of message in bytes.")
      ("threads,t",po::value<size_t>(&threads_number)->default_value(8), "Number of threads.")
      ;

  try {
    po::variables_map vm;
    po::store(po::command_line_parser(argc, argv).options(desc).run(), vm);
    po::notify(vm);

    if (vm.count("help")) {
      std::cout << desc << std::endl;
      return 0;
    }
    if (vm.count("debug")) {
    	PrintLog::ReportingLevel() = PrintLog::FromString("DEBUG");
    }

  } catch (const po::error& e) {
    std::cerr << "Error: " << e.what() << std::endl;
    std::cerr << desc << std::endl;
    return -1;
  }

  int const listening_socket = listen(port);
  if (listening_socket < 0) {
    return -1;
  }

  Print_LOG(logDEBUG) <<"Listening on port "<<port;

  boost::thread_group threads;

  SharedQueue<pollfd> queue;

  Print_LOG(logDEBUG) <<"Creating "<<threads_number<<" threads";

  for (size_t i = 0; i < threads_number; ++i) {
    threads.add_thread(
        new boost::thread(requests_handler, &queue, buffer_size));
  }

  int n_fd = 0;
  while (1) {

    // Blocking
    int new_socket_fd = accept(listening_socket);

    ++n_fd;

    pollfd p;
    p.fd = new_socket_fd;
    p.events = POLLIN;

    Print_LOG(logDEBUG)<< "Accepting connection for socket with fd "<<p.fd;

    // set socket to non-blocking mode
    if (rfcntl(new_socket_fd, F_SETFL, O_NONBLOCK)) {
      std::cerr << "rfcntl\t\t[ERROR] >> errno " << errno << std::endl;
    } else {
      queue.push(p);
    }

  }

  Print_LOG(logDEBUG)<< "Created "<<n_fd<<" sockets";

  threads.join_all();

  rclose(listening_socket);
  return 0;
}
