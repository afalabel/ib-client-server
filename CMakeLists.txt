cmake_minimum_required(VERSION 2.6)

project(IB_Client_Server C CXX)

set(CMAKE_VERBOSE_MAKEFILE OFF)

set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_SOURCE_DIR}/utilities/")

if (CMAKE_COMPILER_IS_GNUCXX)
    message(STATUS "Using GNU c++ compiler")
    # setting flags for GNU compiler
    set(CMAKE_CXX_FLAGS "-pipe -Wall -Wextra")
    set(CMAKE_CXX_FLAGS_RELEASE "-O2")
    set(CMAKE_CXX_FLAGS_DEBUG "-O0 -g3")
    
else (CMAKE_COMPILER_IS_GNUCXX)
    message(FATAL_ERROR "${CMAKE_CXX_COMPILER} is not a supported compiler")
    # exit due to fatal error
    
endif (CMAKE_COMPILER_IS_GNUCXX)

find_package (Threads)
find_package (RDMA)

find_package(Boost REQUIRED COMPONENTS thread system program_options)

include_directories(
  ${RDMA_INCLUDE_DIRS}
  ${IB_Client_Server_SOURCE_DIR}/utilities
)

add_subdirectory(classic)
add_subdirectory(pingpong)
add_subdirectory(rdma)
add_subdirectory(rsockets)
