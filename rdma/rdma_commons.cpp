#include <iostream>

#include <cstring>
#include <errno.h>

#include "rdma_commons.h"

rdma_addrinfo* get_address_info(char* host, char* port)
{
  rdma_addrinfo* address;
  rdma_addrinfo hints;
  memset(&hints, 0, sizeof(hints));
  hints.ai_port_space = RDMA_PS_TCP;
  // if host is null it should be the server
  if (!host) {
    hints.ai_flags = RAI_PASSIVE; // server is passive (and so bind is not needed)
  }
  if (rdma_getaddrinfo(host, port, &hints, &address)) {
    std::cout << "rdma_getaddrinfo\t[ERROR] >> errno " << errno << std::endl;
  } else {
    std::cout << "rdma_getaddrinfo\t[OK]" << std::endl;
  }
  return address;
}

rdma_cm_id* create_endpoint(rdma_addrinfo* address)
{
  ibv_qp_init_attr attr;
  memset(&attr, 0, sizeof(attr));
  attr.cap.max_send_wr = attr.cap.max_recv_wr = 1;
  attr.cap.max_send_sge = attr.cap.max_recv_sge = 1;
  attr.cap.max_inline_data = 0;
  attr.sq_sig_all = 0;
  rdma_cm_id* id;
  int const res = rdma_create_ep(&id, address, NULL, &attr);
  if (res) {
    std::cout << "rdma_create_ep\t\t[ERROR] >> returned " << errno << std::endl;
  } else {
    std::cout << "rdma_create_ep\t\t[OK]" << std::endl;
  }
  return id;
}

void listen(rdma_cm_id* id)
{
  if (rdma_listen(id, NULL)) {
    std::cout << "rdma_listen\t\t[ERROR] >> errno " << errno << std::endl;
  } else {
    std::cout << "rdma_listen\t\t[OK]" << std::endl;
  }
}

rdma_cm_id* get_connection_request(rdma_cm_id* id)
{
  rdma_cm_id* new_id;
  if (rdma_get_request(id, &new_id)) {
    std::cout << "rdma_get_request\t[ERROR] >> errno " << errno << std::endl;
  } else {
    std::cout << "rdma_get_request\t[OK]" << std::endl;
  }
  return new_id;
}

void accept_connection_request(rdma_cm_id* id)
{
  rdma_conn_param cm_params;
  memset(&cm_params, 0, sizeof(cm_params));
  if (rdma_accept(id, &cm_params)) {
    std::cout << "rdma_accept\t\t[ERROR] >> errno " << errno << std::endl;
  } else {
    std::cout << "rdma_accept\t\t[OK]" << std::endl;
  }
}

rdma_event_channel* create_event_channel()
{
  rdma_event_channel* ec = rdma_create_event_channel();
  if (!ec) {
    std::cout << "create_event_channel\t[ERROR] >> errno " << errno
        << std::endl;
  } else {
    std::cout << "create_event_channel\t[OK]" << std::endl;
  }
  return ec;
}

void add_id_to_event_channel(rdma_event_channel* ec, rdma_cm_id* id)
{
  if (rdma_migrate_id(id, ec)) {
    std::cout << "rdma_migrate_id\t\t[ERROR] >> errno " << errno << std::endl;
  } else {
    std::cout << "rdma_migrate_id\t\t[OK]" << std::endl;
  }
}

rdma_cm_event* get_next_event(rdma_event_channel* ec)
{
  rdma_cm_event* event;
  if (rdma_get_cm_event(ec, &event)) {
    std::cout << "rdma_get_cm_event\t[ERROR] >> errno " << errno << std::endl;
  } else {
    std::cout << "rdma_get_cm_event\t[OK]" << std::endl;
  }
  return event;
}
