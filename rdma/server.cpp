#include <iostream>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include <rdma/rdma_cma.h>
#include <rdma/rdma_verbs.h>

#include <boost/thread.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

const size_t BUFFER_SIZE = 1024;
const size_t BUFFER_NUM = 1024;

bool unsafe_done = false;

rdma_event_channel* create_event_channel()
{
  rdma_event_channel* ec = rdma_create_event_channel();
  if (!ec) {
    std::cout << "create_event_channel\t[ERROR] >> errno " << errno
        << std::endl;
  } else {
    std::cout << "create_event_channel\t[OK]" << std::endl;
  }
  return ec;
}

void add_id_to_event_channel(rdma_event_channel* ec, rdma_cm_id* id)
{
  if (rdma_migrate_id(id, ec)) {
    std::cout << "rdma_migrate_id\t\t[ERROR] >> errno " << errno << std::endl;
  } else {
    std::cout << "rdma_migrate_id\t\t[OK]" << std::endl;
  }
}

rdma_cm_event* get_next_event(rdma_event_channel* ec)
{
  rdma_cm_event* event;
  if (rdma_get_cm_event(ec, &event)) {
    std::cout << "rdma_get_cm_event\t[ERROR] >> errno " << errno << std::endl;
  } else {
    std::cout << "rdma_get_cm_event\t[OK]" << std::endl;
  }
  return event;
}

void event_handler(rdma_event_channel* ec)
{
  while (!unsafe_done) {
    rdma_cm_event* event = get_next_event(ec); // blocking
    std::cout << rdma_event_str(event->event) << std::endl;
    switch (event->event) {
    case RDMA_CM_EVENT_DISCONNECTED:
      rdma_disconnect(event->id);
      unsafe_done = true;
      break;
    default:
      ;
    }
    rdma_ack_cm_event(event);
  }
}

void recv_handler(rdma_cm_id* id)
{
  while (!unsafe_done) {
    /* 5th STEP register recv region */
    size_t const recv_msg_size = BUFFER_SIZE * BUFFER_NUM;
    char* recv_msg = new char[recv_msg_size];
    ibv_mr* recv_reg = rdma_reg_msgs(id, recv_msg, recv_msg_size);
    if (!recv_reg) {
      std::cout << "rdma_reg_msgs\t\t[ERROR] >> errno " << errno << std::endl;
      return;
    } else {
      std::cout << "rdma_reg_msgs\t\t[OK]" << std::endl;
    }

    for (size_t i = 0; i < BUFFER_NUM; ++i) {

      char* recv_msg_i = recv_msg + i * BUFFER_SIZE;

      /* 6th STEP receive message */
      int res = rdma_post_recv(id, NULL, recv_msg_i, BUFFER_SIZE, recv_reg);
      if (res) {
        std::cout << "rdma_post_recv\t\t[ERROR] >> errno " << errno
            << std::endl;
        return;
      } else {
        //std::cout << "rdma_post_recv\t\t[OK]" << std::endl;
      }
    }

    for (size_t i = 0; i < BUFFER_NUM; ++i) {
      ibv_wc wc;
      /* 8th STEP retrieve completed work request for receive operation */
      int res = rdma_get_recv_comp(id, &wc);
      if (wc.status == IBV_WC_SUCCESS) {
        std::cout << "Received: " << recv_msg + i * BUFFER_SIZE << std::endl;
      } else if (res < 0) {
        std::cout << "rdma_get_recv_comp\t[ERROR] >> errno " << errno
            << std::endl;
        return;
      }
    }

    rdma_dereg_mr(recv_reg);
  }
}

int main()
{
  /* 1st STEP - get address informations */
  rdma_addrinfo* address;
  rdma_addrinfo hints;
  memset(&hints, 0, sizeof(hints));
  hints.ai_port_space = RDMA_PS_TCP;
  hints.ai_flags = RAI_PASSIVE; // server is passive (and so bind is not needed)
  char port[] = "20079";
  int res = rdma_getaddrinfo(NULL, port, &hints, &address);
  if (res) {
    std::cout << "rdma_getaddrinfo\t[ERROR] >> errno " << errno << std::endl;
    return 1;
  } else {
    std::cout << "rdma_getaddrinfo\t[OK]" << std::endl;
  }

  /* 2nd STEP - create endpoint */
  ibv_qp_init_attr attr;
  memset(&attr, 0, sizeof(attr));
  attr.cap.max_send_wr = attr.cap.max_recv_wr = BUFFER_NUM;
  attr.cap.max_send_sge = attr.cap.max_recv_sge = 1;
  attr.cap.max_inline_data = 0;
  attr.sq_sig_all = 1;
  rdma_cm_id* listen_id;
  res = rdma_create_ep(&listen_id, address, NULL, &attr);
  rdma_freeaddrinfo(address);
  if (res) {
    std::cout << "rdma_create_ep\t\t[ERROR] >> errno " << errno << std::endl;
    return 1;
  } else {
    std::cout << "rdma_create_ep\t\t[OK]" << std::endl;
  }

  /* 3rd STEP listen */
  res = rdma_listen(listen_id, NULL);
  if (res) {
    std::cout << "rdma_listen\t\t[ERROR] >> errno " << errno << std::endl;
    return 1;
  } else {
    std::cout << "rdma_listen\t\t[OK]" << std::endl;
  }

  rdma_event_channel* ec = create_event_channel();

  while (1) {

    /* 4th STEP get request */
    rdma_cm_id* new_id;
    res = rdma_get_request(listen_id, &new_id);
    if (res) {
      std::cout << "rdma_get_request\t[ERROR] >> errno " << errno << std::endl;
      return 1;
    } else {
      std::cout << "rdma_get_request\t[OK]" << std::endl;
    }

    /* 7th STEP accept */
    res = rdma_accept(new_id, NULL);
    if (res) {
      std::cout << "rdma_accept\t\t[ERROR] >> errno " << errno << std::endl;
      return 1;
    } else {
      std::cout << "rdma_accept\t\t[OK]" << std::endl;
    }

    unsafe_done = false;

    boost::thread event_handler_thread(event_handler, ec);
    boost::thread recv_handler_thread(recv_handler, new_id);

    add_id_to_event_channel(ec, new_id);

    event_handler_thread.join();
    recv_handler_thread.join();

    /* Last STEP disconnect and free resources */
    //rdma_disconnect(new_id);
    rdma_destroy_ep(new_id);
  }

  rdma_destroy_ep(listen_id);

  return 0;
}
