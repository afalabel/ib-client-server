#include <iostream>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include <boost/thread.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

#include <rdma/rdma_cma.h>
#include <rdma/rdma_verbs.h>

const size_t BUFFER_SIZE = 1024;
const size_t BUFFER_NUM = 1024;

int main()
{
  /* 1st STEP - get address informations */
  rdma_addrinfo* address;
  rdma_addrinfo hints;
  memset(&hints, 0, sizeof(hints));
  hints.ai_port_space = RDMA_PS_TCP;
  char host[] = "192.168.1.12";
  char port[] = "20079";
  int res = rdma_getaddrinfo(host, port, &hints, &address);
  if (res) {
    std::cout << "rdma_getaddrinfo\t[ERROR] >> returned " << errno << std::endl;
    return 1;
  } else {
    std::cout << "rdma_getaddrinfo\t[OK]" << std::endl;
  }

  /* 2nd STEP - create endpoint */
  ibv_qp_init_attr attr;
  memset(&attr, 0, sizeof(attr));
  attr.cap.max_send_wr = attr.cap.max_recv_wr = BUFFER_NUM;
  attr.cap.max_send_sge = attr.cap.max_recv_sge = 1;
  attr.cap.max_inline_data = 0;
  attr.sq_sig_all = 1;
  rdma_cm_id* id;
  res = rdma_create_ep(&id, address, NULL, &attr);
  rdma_freeaddrinfo(address);
  if (res) {
    std::cout << "rdma_create_ep\t\t[ERROR] >> errno " << errno << std::endl;
    return 1;
  } else {
    std::cout << "rdma_create_ep\t\t[OK]" << std::endl;
  }

  /* 3rd STEP register send region */
  size_t const send_msg_size = BUFFER_SIZE * BUFFER_NUM;
  char* send_msg = new char[send_msg_size];
  ibv_mr* send_reg = rdma_reg_msgs(id, send_msg, send_msg_size);
  if (!send_reg) {
    std::cout << "rdma_reg_msgs\t\t[ERROR] >> errno " << errno << std::endl;
    return 1;
  } else {
    std::cout << "rdma_reg_msgs\t\t[OK]" << std::endl;
  }

  /* 4th STEP connection */
  res = rdma_connect(id, NULL);
  if (res) {
    std::cout << "rdma_connect\t\t[ERROR] >> errno " << errno << std::endl;
    return 1;
  } else {
    std::cout << "rdma_connect\t\t[OK]" << std::endl;
  }

  boost::posix_time::ptime const t1 =
      boost::posix_time::microsec_clock::local_time();

  for (size_t i = 0; i < BUFFER_NUM; ++i) {

    char* send_msg_i = send_msg + i * BUFFER_SIZE;

    sprintf(send_msg_i, "%zu", i);

    /* 5th STEP send message */
    res = rdma_post_send(id, NULL, send_msg_i, BUFFER_SIZE, send_reg, NULL);
    if (res) {
      std::cout << "rdma_post_send\t\t[ERROR] >> errno " << errno << std::endl;
      return 1;
    }

  }

  for (size_t i = 0; i < BUFFER_NUM; ++i) {
    ibv_wc wc;
    res = rdma_get_send_comp(id, &wc);
    if (res < 0) {
      std::cout << "rdma_get_send_comp\t[ERROR] >> errno " << errno
          << std::endl;
      return 1;
    }
  }

  boost::posix_time::ptime const t2 =
      boost::posix_time::microsec_clock::local_time();

  boost::posix_time::time_duration const diff = t2 - t1;
  std::cout << "Sent " << BUFFER_SIZE * BUFFER_NUM << " bytes in "
      << diff.total_microseconds() << " us" << std::endl;

  /* Last STEP disconnect and free resources */
  rdma_disconnect(id);
  rdma_dereg_mr(send_reg);
  rdma_destroy_ep(id);

  return 0;
}
