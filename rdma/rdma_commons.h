#ifndef UTILITIES_RDMACOMMONS_H
#define UTILITIES_RDMACOMMONS_H

#include <rdma/rdma_cma.h>
#include <rdma/rdma_verbs.h>

rdma_addrinfo* get_address_info(char* host, char* port);
rdma_cm_id* create_endpoint(rdma_addrinfo* address);
void listen(rdma_cm_id* id);
rdma_cm_id* get_connection_request(rdma_cm_id* id);
void accept_connection_request(rdma_cm_id* id);
rdma_event_channel* create_event_channel();
void add_id_to_event_channel(rdma_event_channel* ec, rdma_cm_id* id);
rdma_cm_event* get_next_event(rdma_event_channel* ec);

#endif
